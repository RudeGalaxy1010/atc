﻿namespace DFSM
{
    public class Condition
    {
        public State NextState { get; private set; }
        private readonly Func<string, bool> _condition;

        public Condition(Func<string, bool> condition, State nextState)
        {
            NextState = nextState;
            _condition = condition;
        }

        public bool IsMet(string input)
        {
            return _condition(input);
        }
    }
}
