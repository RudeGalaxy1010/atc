﻿using System.Text;

namespace DFSM
{
    public static class ArrayExtensions
    {
        public static string ToStringView<T>(this T[] values)
        {
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < values.Length; i++)
            {
                stringBuilder.Append(values[i]);

                if (i == values.Length - 1)
                {
                    break;
                }

                stringBuilder.Append(',');
                stringBuilder.Append(' ');
            }

            return stringBuilder.ToString();
        }
    }
}
