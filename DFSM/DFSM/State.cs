﻿namespace DFSM
{
    public class State
    {
        public bool IsFinal { get; private set; }

        private Action<State> _exitCallback;
        private Condition[] _conditions;

        public State AddExitCallback(Action<State> exitCallback)
        {
            _exitCallback = exitCallback;
            return this;
        }

        public State AddConditions(params Condition[] conditions)
        {
            _conditions = conditions;
            return this;
        }

        public State MarkAsFinal()
        {
            IsFinal = true;
            return this;
        }

        public void Enter(string input, State fromState = null)
        {
            if (fromState == this)
            {
                Console.WriteLine("Cycle found!\nExiting...");
                Exit();
                return;
            }
            
            if (IsFinal && fromState != null)
            {
                Console.WriteLine("Final state!\nExiting...");
                Exit();
                return;
            }

            Condition metCondition = _conditions.FirstOrDefault(c => c.IsMet(input));

            if (metCondition == null)
            {
                Console.WriteLine("Failed to find condition to met!\nExiting...");
                Exit();
                return;
            }

            metCondition.NextState.Enter(input, this);
        }

        private void Exit()
        {
            _exitCallback?.Invoke(this);
        }
    }
}
