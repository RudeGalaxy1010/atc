﻿namespace DFSM
{
    public class StateMachine
    {
        private const int MinInputLength = 2;

        private readonly char[] _alphabet;
        private readonly State[] _states;

        public StateMachine(char[] alphabet, State initialState, params State[] states)
        {
            _alphabet = alphabet;
            _states = new State[] { initialState }.Union(states).ToArray();

            foreach (var state in _states)
            {
                state.AddExitCallback(Stop);
            }
        }

        public void Work()
        {
            while (true)
            {
                string word = ReadInput();
                _states[0].Enter(word);
                Console.ReadKey();
            }
        }

        public void Stop(State finalState)
        {
            if (finalState.IsFinal)
            {
                Console.WriteLine("Pass!");
            }
            else
            {
                Console.WriteLine("Not pass!");
            }
        }

        private string ReadInput()
        {
            bool passed = false;
            string input;

            do
            {
                Console.Clear();
                Console.WriteLine($"Алфавит: {_alphabet.ToStringView()}");
                Console.Write("Введите слово: ");
                input = Console.ReadLine();
                
                if (string.IsNullOrEmpty(input))
                {
                    Console.WriteLine("Слово не может быть пустым!");
                    Console.ReadKey();
                    continue;
                }

                if (input.Length < MinInputLength)
                {
                    Console.WriteLine($"Слово не может быть короче {MinInputLength} символов!");
                    Console.ReadKey();
                    continue;
                }

                if (input.Any(v => !_alphabet.Contains(v)))
                {
                    Console.WriteLine("Слово не может содержать символы не из алфавита");
                    Console.ReadKey();
                    continue;
                }

                passed = true;
            }
            while (!passed);

            return input;
        }
    }
}
