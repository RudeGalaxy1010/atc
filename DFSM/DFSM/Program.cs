﻿namespace DFSM
{
    public class Program
    {
        static void Main(string[] args)
        {
            // Создание алфавита
            char[] alphabet = new char[] { '0', '1' };

            // Создание состояний
            State state1 = new State().MarkAsFinal();
            State state2 = new State();
            State state3 = new State();

            // Создание условий перехода
            Condition lastSymbolEqualsZero = new Condition((s) => 
            { 
                Console.WriteLine("State 1: Last symbol is 0\nGoing to: State 3"); 
                return s.Length > 0 && s[s.Length - 1] == '0'; 
            }, state3);
            Condition preLastSymbolEqualsZero = new Condition((s) => 
            { 
                Console.WriteLine("State 3: Prelast symbol is 0\nGoing to: State 1"); 
                return s.Length > 1 && s[s.Length - 2] == '0'; 
            }, state1);
            Condition lastSymbolEqualsOne = new Condition((s) => 
            { 
                Console.WriteLine("State 1: Last symbol is 1\nGoing to: State 2");
                return s.Length > 0 && s[s.Length - 1] == '1'; 
            }, state2);
            Condition preLastSymbolEqualsOne = new Condition((s) => 
            {
                Console.WriteLine("State 3: Prelast symbol is 1\nGoing to: State 2");
                return s.Length > 1 && s[s.Length - 2] == '1'; 
            }, state2);
            Condition alwaysFalse = new Condition((s) => 
            {
                Console.WriteLine($"State 2: Last symbols is {s.Substring(s.Length - 2, 2)}\nGoing to: State 2");
                return false; 
            }, state2);

            // Конфигарация состояний
            state1.AddConditions(lastSymbolEqualsZero, lastSymbolEqualsOne);
            state2.AddConditions(alwaysFalse);
            state3.AddConditions(preLastSymbolEqualsZero, preLastSymbolEqualsOne);

            // Конфигурация детерменированного конечного автомата
            StateMachine stateMachine = new StateMachine(alphabet, state1, state2, state3);
            // Запуск автомата
            stateMachine.Work();
        }
    }
}