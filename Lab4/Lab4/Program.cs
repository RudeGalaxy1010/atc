﻿using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;

namespace Lab4
{
    public static class Program
    {
        private const string FileName = "tel.html";
        private const string Pattern1 = @"\d{3}-\d{2}-\d{2}"; // 111-11-11
        private const string Pattern2 = @"\+7 \(\d{3}\) \d{3}-\d{2}-\d{2}"; // +7 (111) 111-11-11

        private static long _fileReadTime;
        private static long _searchTime;
        private static long _outputTime;

        public static void Main(string[] args)
        {
            StringBuilder sb = new StringBuilder();

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            string filePath = $"{Directory.GetCurrentDirectory()}/{FileName}";
            string data = ReadFile(filePath);
            stopwatch.Stop();
            _fileReadTime = stopwatch.ElapsedMilliseconds;

            stopwatch.Restart();
            var tels1 = Regex.Matches(data, Pattern1).Cast<Match>();
            var tels2 = Regex.Matches(data, Pattern2).Cast<Match>();
            var tels = tels1.Union(tels2).Select(t => t.Value).Distinct();
            stopwatch.Stop();
            _searchTime = stopwatch.ElapsedMilliseconds;

            stopwatch.Restart();

            foreach (var tel in tels)
            {
                sb.AppendLine(tel);
            }

            Console.WriteLine(sb.ToString());
            stopwatch.Stop();
            _outputTime = stopwatch.ElapsedMilliseconds;

            Console.WriteLine();
            Console.WriteLine($"Найдено уникальных номеров: {tels.Count()}");
            Console.WriteLine();
            Console.WriteLine("Затраченое время:");
            Console.WriteLine($"Чтение файла: {_fileReadTime} мс");
            Console.WriteLine($"Поиск совпадений: {_searchTime} мс");
            Console.WriteLine($"Вывод в консоль: {_outputTime} мс");
        }

        private static string ReadFile(string fileName)
        {
            return File.ReadAllText(fileName);
        }
    }
}
