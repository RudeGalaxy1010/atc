﻿namespace Lab5
{
    public static class Program
    {
        public static void Main(params string[] args)
        {
            UserStringReader stringreader = new UserStringReader();
            Parser parser = new Parser(stringreader);
            parser.Work();
        }
    }
}