﻿using System.Text.RegularExpressions;

namespace Lab5
{
    public class Parser
    {
        private readonly UserStringReader _stringreader;

        private readonly string[] arithmeticalOperators = new string[] { "+", "-", "/", "*", "%" };
        private readonly string[] logicalOperators = new string[] { "||", "&&", "!", "^" };
        private readonly string[] binaryOperators = new string[] { "|", "&", "^", "~", "<<", ">>" };
        private readonly string[] comparsionOperator = new string[] { "==", "!=", ">", "<", "<=", ">=" };

        public Parser(UserStringReader stringreader)
        {
            _stringreader = stringreader;
        }

        public void Work()
        {
            while (true)
            {
                Console.Clear();
                string input = _stringreader.Read();
                ParseTokensAndWrite(input);
                Console.ReadKey();
            }
        }

        private void ParseTokensAndWrite(string input)
        {
            string[] words = Regex.Split(input, @"\s+").Where(s => !string.IsNullOrEmpty(s)).ToArray();

            foreach (var word in words)
            {
                if (logicalOperators.Contains(word))
                {
                    Console.WriteLine($"{word}\t logical operator");
                    continue;
                }

                if (binaryOperators.Contains(word))
                {
                    Console.WriteLine($"{word}\t binary operator");
                    continue;
                }

                if (comparsionOperator.Contains(word))
                {
                    Console.WriteLine($"{word}\t comparsion operator");
                    continue;
                }

                if (arithmeticalOperators.Contains(word))
                {
                    Console.WriteLine($"{word}\t arithmetical operator");
                    continue;
                }

                if (Regex.IsMatch(word, @"'\w.+'"))
                {
                    Console.WriteLine($"{word}\t string");
                    continue;
                }

                if (Regex.IsMatch(word, @"\d+"))
                {
                    Console.WriteLine($"{word}\t number");
                    continue;
                }
                
                if (Regex.IsMatch(word, @"\w.+"))
                {
                    Console.WriteLine($"{word}\t variable");
                    continue;
                }
            }
        }
    }
}