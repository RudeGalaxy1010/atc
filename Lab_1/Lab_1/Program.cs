﻿using System;
using System.Diagnostics;
using System.Linq;

namespace Lab_1
{
    public class Program
    {
        private static Random random = new Random();
        private static Tuple<char, string>[] replaceRules = new Tuple<char, string>[]
        {
            Tuple.Create('S', "aSb"),
            Tuple.Create('S', "cFc"),
            Tuple.Create('F', "cFc"),
            Tuple.Create('F', "")
        };

        public static void Main(string[] args)
        {
            Stopwatch stopwatch = new Stopwatch();
            while (true)
            {
                Console.Clear();
                stopwatch.Reset();
                int wordsCount = ReadInput();

                Console.WriteLine("Слова: ");

                stopwatch.Start();
                for (int i = 0; i < wordsCount; i++)
                {
                    string randomString = GenerateRandomString(10);
                    string word = GenerateWord(randomString);
                    Console.WriteLine($"{randomString} -> {word}");
                }
                stopwatch.Stop();

                Console.WriteLine($"\nDone! {stopwatch.Elapsed.TotalMilliseconds} ms took");
                Console.ReadKey();
            }
        }

        private static int ReadInput()
        {
            string input;
            int result;

            do
            {
                Console.WriteLine("Введите количество слов: ");
                input = Console.ReadLine();
            }
            while (int.TryParse(input, out result) == false || result < 0);

            return result;
        }

        public static string GenerateWord(string value)
        {
            foreach (var replaceRule in replaceRules)
            {
                for (int i = 0; i < value.Length; i++)
                {
                    char symbol = value[i];

                    if (replaceRule.Item1 != symbol)
                    {
                        continue;
                    }

                    value = value.Remove(i, 1);
                    value = value.Insert(i, replaceRule.Item2);
                    i += replaceRule.Item2.Length - 1;
                }
            }

            return value;
        }

        public static string GenerateRandomString(int length)
        {
            const string chars = "SF";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
