﻿namespace DFSM
{
    public class StateMachine
    {
        private readonly int[] _alphabet;
        private readonly int _productCost = 5;

        private int _value;

        public StateMachine(int[] alphabet, int productCost)
        {
            _alphabet = alphabet;
            _productCost = productCost;
        }

        public void Work()
        {
            while (true)
            {
                _value = 0;
                Console.Clear();
                Console.WriteLine($"Состояние {_value}");

                while (true)
                {
                    int money = ReadInput();
                    _value += money;

                    if (_value >= 5)
                    {
                        Console.WriteLine($"Состояние {_value} | Перейти в состояние 0");
                        Console.WriteLine($"Выдать товар | Сдача: {_value - _productCost}");
                        break;
                    }
                    else
                    {
                        Console.WriteLine($"Состояние {_value} | Перейти в состояние {_value + money}");
                    }
                }

                Console.ReadKey();
            }
        }

        private int ReadInput()
        {
            bool passed = false;
            string input;
            int money = 0;

            do
            {
                Console.WriteLine($"Доступные монеты: {_alphabet.ToStringView()}");
                Console.Write("Введите количество денег: ");
                input = Console.ReadLine();
                
                if (string.IsNullOrEmpty(input))
                {
                    Console.WriteLine("Количество денег не может быть пустым!");
                    Console.ReadKey();
                    continue;
                }

                if (int.TryParse(input, out money) == false)
                {
                    Console.WriteLine("Некорректный номинал монеты!");
                    Console.ReadKey();
                    continue;
                }

                if (!_alphabet.Contains(money))
                {
                    Console.WriteLine("Монеты такого номинала не доступны!");
                    Console.ReadKey();
                    continue;
                }

                passed = true;
            }
            while (!passed);

            return money;
        }
    }
}
