﻿namespace DFSM
{
    public class Program
    {
        static void Main(string[] args)
        {
            // Задаём алфавит
            int[] alphabet = new int[] { 1, 2, 5};
            // Задаём стоимость товара
            int productCost = 5;

            // Конфигурация автомата Мили
            StateMachine stateMachine = new StateMachine(alphabet, productCost);
            // Запуск автомата
            stateMachine.Work();
        }
    }
}